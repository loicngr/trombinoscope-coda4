# Trombinoscope Coda4

Le Trombinoscope de CODA 4 par Loïc et Azraël!
Nous faisons un trombinoscope pour la session 4 de la formation de développeur d'applications web et mobile. Le trombinoscope comprendra les facilitateurs et les stagiaires.

**Sur mobile : **
L'utilisateur arrive sur la page d'accueil où il voit le header suivie d'une colonne d'images rondes. Les images ont un border de couleur qui varie pour differencier les facilitateurs des stagiaires.
Il peut appuyer sur chaque image pour arriver sur une fiche descriptive de la personne qu'il aura selectionné.
Dans la fiche descriptive on retrouvera l'image de la personne tout en haut suivie de son nom et d'une citation culte d'elle.
Il y aura ensuite une catégories caractéristiques et compétences.
Enfin tout en bas à droite il y aura un bouton retour permettant de revenir à la page d'accueil.

**Sur desktop : **
L'utilisateur arrive sur la page d'accueil où il voit le header suivie d'une première rangée de deux images avec "Facilitateurs" écrit en dessous puis trois rangées de quatres colonnes avec "Stagiaires" écrit en dessous differencier les facilitateurs des stagiaires.
Il peut cliquer sur chaque image pour arriver sur une fiche descriptive de la personne qu'il aura selectionné.
Dans la fiche descriptive on retrouvera l'image de la personne tout en haut suivie de son nom et d'une citation culte d'elle.
Il y aura ensuite une catégories caractéristiques et compétences.
Enfin tout en bas à droite il y aura un bouton retour permettant de revenir à la page d'accueil.

Le design est responsive c'est à dire que le maximum d'images par rangée est de quatre mais plus la taille de l'écran diminue plus le nombre d'images par rangée diminuera.