/**
 * Async / Await ?? (français)
 * La déclaration async function définit une fonction asynchrone qui renvoie un objet AsyncFunction. 
 * Une fonction asynchrone est une fonction qui s'exécute de façon asynchrone grâce à la boucle d'évènement 
 * en utilisant une promesse (Promise) comme valeur de retour.
 * 
 * Une fonction async peut contenir une expression await qui interrompt l'exécution de la fonction asynchrone 
 * et attend la résolution de la promesse passée Promise. 
 * La fonction asynchrone reprend ensuite puis renvoie la valeur de résolution.
 * 
 * Le mot-clé await est uniquement valide au sein des fonctions asynchrones. 
 * Si ce mot-clé est utilisé en dehors du corps d'une fonction asynchrone,
 * cela provoquera une exception SyntaxError.
 */

/**
 * Async Load Json file
 * @returns {object|boolean} File loaded or eror when try to load file
 */
async function ft_loadJson() {
    // Try Catch documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/try...catch
    try {
        // Fetch documentation : https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch
        const RESPONSE = await fetch('./data.json', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const DATA_JSON = await RESPONSE.json();
        return DATA_JSON;
    } catch (error) {
        // If an error occurred when opening a file
        // we return an false bool
        return false;
    }
}

/**
 * Generate elements in HTML DOM
 * @param {object} users
 */
function ft_generateElements(users) {
    // Get all falicitateur users in users object
    // In filter loop we return true if user is an 'falicitateur'
    // Filter documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/filter
    const FALICITATEURS = Object.entries(users).filter(user => {
        // We use [1] because [0] is a key of user object and [1] content of user object
        if (user[1].status === 'falicitateur') {
            return true;
        }
        return false;
    });
    const STAGIAIRES = Object.entries(users).filter(user => {
        if (user[1].status === 'stagiaire') {
            return true;
        }
        return false;
    });

    // Loop in users Object (data.json file)
    for (user in users) {
        if (users.hasOwnProperty(user)) {
            // Create DOM Element
            let article = document.createElement('article');
            let linkimage = document.createElement('a');
            let image = document.createElement('img');

            // Setup link and avatar
            linkimage.setAttribute('href', user + '.html');
            image.setAttribute('src', users[user].avatar);

            // If user is Stagiaire or Falicitateur
            if (users[user].status !== 'stagiaire') {
                image.classList.add('teach');

                linkimage.appendChild(image);
                article.appendChild(linkimage);
                let list = document.getElementsByClassName('blocs')[0];
                list.insertBefore(article, list.childNodes[0]);

                // If the number of .teach class is the same as the number of FALICITATEURS
                if (document.querySelectorAll('.teach').length === FALICITATEURS.length) {
                    let newline = document.createElement('br');
                    list.insertBefore(newline, list.childNodes[FALICITATEURS.length]);
                    
                    let title = document.createElement('h1');
                    title.innerText = 'Falicitateurs';
                    list.insertBefore(title, list.childNodes[FALICITATEURS.length]);
                }
            } else {
                linkimage.appendChild(image);
                article.appendChild(linkimage);
                document.getElementsByClassName('blocs')[0].appendChild(article);
            }

            // If articles length minus FALICITATEURS length (2) is equal a STAGIAIRES length (12)
            if (document.querySelectorAll('article').length - FALICITATEURS.length === STAGIAIRES.length) {
                // We get .blocs element, [0] for get first .blocs element in HTML DOM
                // We create an h1 title
                // We change h1 title content with 'Stagiaires'
                // We append title in .blocs element
                let list = document.getElementsByClassName('blocs')[0];
                let title = document.createElement('h1');
                title.innerText = 'Stagiaires';
                list.appendChild(title);
            }
        }
    }
}

/**
 * Main function
 */
async function main() {
    // Call function ft_loadJson for load JSON file
    const JSON_DATA_RESPONSE = await ft_loadJson();

    // If file loaded
    if (JSON_DATA_RESPONSE) {
        ft_generateElements(JSON_DATA_RESPONSE);
    }
}

// Call main function
main();
